export namespace FLogger {
  type Outcome = "pass" | "fail";
  type RolloverOutcome = {
    compressed: boolean;
    processedPaths: string[];
    errors: {
      message: string;
      meta: any;
    }[];
  };
  type ArchiveOutcome = {
    readFileOutcome: FileLogger.Outcome;
    compressionOutcome: FileLogger.Outcome;
    cleanedPreviousOutcome: FileLogger.Outcome;
    errors: Error[];
  };

  type LogRecordOutcome = {
    message: string | null;
    outcome: FileLogger.Outcome;
  };

  type AccessToPathOutcome = {
    outcome: FileLogger.Outcome;
    errors: Error[];
  };
}
