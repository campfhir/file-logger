import { FileLogger } from "../src";
import fs from "node:fs/promises";
import path from "node:path";
import util from "node:util";
import { DateTime } from "luxon";
import { constants } from "node:fs";
const testLogDir = "./test-logs";
describe("Given a daily rotating logger with compression instance with a valid path", () => {
  const logger = FileLogger.create(testLogDir, {
    rotateDaily: true,
    compressPreviousLogs: true,
  });

  before("Setup - Initialize Empty debug.log file", async () => {
    await fs.mkdir(testLogDir, { recursive: true });
    const initialDebugPath = path.resolve(testLogDir, "debug.log");
    const initialInfoPath = path.resolve(testLogDir, "info.log");
    const initialWarnPath = path.resolve(testLogDir, "warn.log");
    const initialErrorPath = path.resolve(testLogDir, "error.log");
    const setupMessage = util.format("[%s] Hello", DateTime.local().toISO());
    await fs.appendFile(initialDebugPath, setupMessage);
    await fs.appendFile(initialInfoPath, setupMessage);
    await fs.appendFile(initialWarnPath, setupMessage);
    await fs.appendFile(initialErrorPath, setupMessage);
    logger["history"].push(path.join(testLogDir, `debug.log`));
    logger["history"].push(path.join(testLogDir, `info.log`));
    logger["history"].push(path.join(testLogDir, `error.log`));
    logger["history"].push(path.join(testLogDir, `warn.log`));
    logger["previousLogDate"] = DateTime.local().minus({ day: 1 });
    return;
  });

  describe("When executing log to a record", () => {
    const yesterdaysDate = DateTime.local().minus({ day: 1 }).toISODate();
    const archivedInfoLogPath = path.resolve(
      testLogDir,
      `info_${yesterdaysDate}.log.gz`
    );
    const archivedWarnLogPath = path.resolve(
      testLogDir,
      `warn_${yesterdaysDate}.log.gz`
    );
    const archivedDebugLogPath = path.resolve(
      testLogDir,
      `debug_${yesterdaysDate}.log.gz`
    );
    const archivedErrorLogPath = path.resolve(
      testLogDir,
      `error_${yesterdaysDate}.log.gz`
    );
    it("Then should record without error", async () => {
      await logger.debug("Hello %s", "scott");
      await logger.debug("Hello Sir");
      await logger.debug("Greetings %s %s", "scott", "eremia-roden");
      await logger.info("Hello Again");
      await logger.error("Something is wrong %O", {
        error: { status: 400, message: "bad request" },
      });
      await logger.warn("this could be a problem", "something else here");

      await fs.access(archivedDebugLogPath, constants.R_OK);
      await fs.access(archivedInfoLogPath, constants.R_OK);
      await fs.access(archivedErrorLogPath, constants.R_OK);
      await fs.access(archivedWarnLogPath, constants.R_OK);
      return;
    });
  });

  after("Cleanup - Remove test-logs directory", async () => {
    await fs.rm(testLogDir, { recursive: true, force: true });
  });
});
