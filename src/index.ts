import fs from "node:fs/promises";
import util from "node:util";
import { gzip } from "node:zlib";
import path from "path";
import { DateTime } from "luxon";
import { createWriteStream, WriteStream } from "node:fs";
import d from "debug";
import { FLogger } from "../typings";

const logger = d("FileLogger");
const archiveLogger = logger.extend("archive");
const gzipLogger = archiveLogger.extend("gzip");
const dailyLog = logger.extend("dailyLog");
const recordLogger = logger.extend("record");
const logLogger = logger.extend("log");
const checkLoggingPathLogger = logger.extend("checkLoggingPath");

export class FileLogger {
  private loggingDirectory: string;
  private rotateDaily: boolean;
  private compressPreviousLogs: boolean;
  private previousLogDate: DateTime;
  private history: string[];
  private pathVerified: FLogger.Outcome | null;
  private logStreams: {
    [level: string]: WriteStream;
  };

  /**
   *
   * @param loggingDirectory The directory where log files are stored
   * @param options
   * @param options.rotateDaily If true each day logs will rollover and be renamed to the previous date default: `false`
   * @param options.compressPreviousLogs If rotate `true` and compressPreviousLogs `true` then previous log files will be compressed in a Gzip. Ignored if rotateDaily `false`
   */
  private constructor(
    loggingDirectory: string,
    options?: { rotateDaily?: boolean; compressPreviousLogs?: boolean }
  ) {
    this.rotateDaily = options?.rotateDaily ?? false;
    this.loggingDirectory = path.resolve(loggingDirectory);
    this.compressPreviousLogs =
      options?.rotateDaily === true
        ? options.compressPreviousLogs ?? false
        : false;
    this.previousLogDate = DateTime.local();
    this.history = [];
    this.logStreams = {};
    this.pathVerified = null;
  }

  /**
   *
   * @param loggingDirectory The path where log files are stored
   */
  static create(loggingDirectory: string): FileLogger;
  /**
   *
   * @param loggingDirectory The path where log files are stored
   * @param options.rotateDaily Set to `false` to maintain on log file
   * @param options.compressPreviousLogs will be ignored when rotateDaily is `false`
   */
  static create(
    loggingDirectory: string,
    options: { rotateDaily: false; compressPreviousLogs: never }
  ): FileLogger;
  /**
   *
   * @param loggingDirectory
   * @param options.rotateDaily Set to `true` to maintain rollover daily log files
   * @param options.compressPreviousLogs compress the rolled over log files in a Gzip when `true`
   */
  static create(
    loggingDirectory: string,
    options: { rotateDaily: true; compressPreviousLogs?: boolean }
  ): FileLogger;
  static create(
    loggingDirectory: string,
    options?: { rotateDaily?: boolean; compressPreviousLogs?: boolean }
  ): FileLogger {
    const logger = new FileLogger(loggingDirectory, options);
    process.on("exit", () => {
      logger.closeStreams();
    });
    return logger;
  }

  private closeStreams() {
    for (let stream of Object.values(this.logStreams)) {
      stream.end();
    }
  }

  private async rollover(
    previousDateTime: DateTime,
    history: string[]
  ): Promise<void> {
    for (const ind in history) {
      const prvPath = history[ind];
      if (this.compressPreviousLogs) {
        await this.archive(this.previousLogDate, prvPath);
        continue;
      }
      const previousDate = previousDateTime.toISODate();
      const newPath = prvPath.replace(/\.log$/, `_${previousDate}.log`);
      await fs.rename(prvPath, newPath);
    }
    this.previousLogDate = DateTime.local();
    return;
  }

  /**
   *
   * @param existingLogFilePath Path to the file to be archived
   * @returns The path to the newly archived file
   */
  private async archive(
    previousDate: DateTime,
    existingLogFilePath: string
  ): Promise<string> {
    const log = archiveLogger;
    const archiveDest = existingLogFilePath.replace(
      /.log$/,
      `_${previousDate.toISODate()}.log.gz`
    );
    log("Attempting Archiving %s => %s", existingLogFilePath, archiveDest);

    return new Promise<string>(async (resolve, reject) => {
      try {
        const logData = await fs.readFile(existingLogFilePath);
        log("Read %s data", existingLogFilePath);
        await this.gzip(logData, archiveDest);
        await fs.rm(existingLogFilePath);
        resolve(archiveDest);
        return;
      } catch (err) {
        reject(err);
        return;
      }
    });
  }

  private async gzip(data: Buffer, destination: string): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      gzip(data, (err, buff) => {
        const log = gzipLogger;
        log("Compressing data");
        if (err != null) {
          reject(err);
          return;
        }

        log("Creating writestream to save compressed files to %s", destination);
        const stream = createWriteStream(destination);
        stream.write(buff, (err) => {
          if (err) {
            reject(err);
            return;
          }
          stream.end(() => {
            log("Wrote compressed file", destination);
            resolve(destination);
          });
        });

        return;
      });
    });
  }

  private async dailyLog(level: string, message: string): Promise<string> {
    const log = dailyLog;
    return new Promise<string>((resolve, reject) => {
      log("Level %s, Message %s", level, message);
      const dateToday = DateTime.local();
      const fileName = `${level}.log`;
      const previousDate = this.previousLogDate;
      if (dateToday.diff(previousDate, ["days"]).days >= 1) {
        log("Previous Date %O", previousDate.toISODate());
        const levelStream = this.logStreams[level];
        if (levelStream) {
          log("Closing %s stream", level);
          levelStream.end(null, () => {
            this.rollover(this.previousLogDate, this.history)
              .then(() => resolve(message))
              .catch(reject);
            return;
          });
        } else {
          log("No Stream to close");
          this.rollover(previousDate, this.history)
            .then(() => resolve(message))
            .catch(reject);
        }
        return;
      }

      this.record(level, fileName, message)
        .then(() => resolve(message))
        .catch(reject);
    });
  }

  private async record(
    level: string,
    logFileName: string,
    message: string
  ): Promise<string> {
    const log = recordLogger;
    log(
      "Level: %s, Log File Name: %s, Message %s",
      level,
      logFileName,
      message
    );
    return new Promise<string>(async (resolve, reject) => {
      const logFilePath = path.join(this.loggingDirectory, logFileName);
      log("Log File Path: %s", logFilePath);
      try {
        if (this.pathVerified == null) {
          await this.checkLoggingPath(this.loggingDirectory);
        }
        const previousLevelStream = this.logStreams[level];
        let stream: WriteStream;
        if (previousLevelStream != null) {
          log("WriteStream exists for this level: %s", level);
          stream = previousLevelStream;
        } else {
          log("Create a WriteStream level: %s", level);
          stream = createWriteStream(logFilePath, { flags: "a" });
        }
        this.logStreams[level] = stream;
        stream.write(message, (err) => {
          if (err) {
            reject(err);
            return;
          }
          log("Appending path to history: %s", logFilePath);
          this.history.push(logFilePath);
          resolve(message);
        });
      } catch (err) {
        reject(err);
        return;
      }
    });
  }

  /**
   * @description Checks if the path is accessible for logging
   *
   * @return {boolean} True = path is accessible to log False = Path is inaccessible
   */
  private async checkLoggingPath(loggingPath: string): Promise<void> {
    const log = checkLoggingPathLogger;
    try {
      log("Checking access for directory: %s", loggingPath);
      await fs.access(loggingPath);
      return;
    } catch (err) {
      try {
        log("Attempting to create the logging directory %s", loggingPath);
        let dir = await fs.mkdir(loggingPath, { recursive: true });
        log("Created the logging directory %s", dir);
        return;
      } catch (err) {
        throw err;
      }
    }
  }
  async log(
    level: string,
    raw_message: string,
    ...args: any[]
  ): Promise<string> {
    const log = logLogger;
    log("Level: [%s] Template: `%s` Args: %O", level, raw_message, args);
    // remove any color
    const colorlessMessageTemplate = raw_message.replace(
      /\x1B[[(?);]{0,2}(;?\d)*./g,
      ""
    );
    const template = `[%s] - ${colorlessMessageTemplate}\n`;
    const timestamp = DateTime.local().toISO();
    const message = util.format(template, timestamp, ...args);
    if (this.rotateDaily) {
      log("Logging is setup for rotating");
      return this.dailyLog(level, message);
    }
    log("Write log to file");
    const fileName = `${level}.log`;
    return this.record(level, fileName, message);
  }
  async info(message: string, ...args: any[]): Promise<string> {
    return this.log("info", message, ...args);
  }
  async error(message: string, ...args: any[]): Promise<string> {
    return this.log("error", message, ...args);
  }
  async warn(message: string, ...args: any[]): Promise<string> {
    return this.log("warn", message, ...args);
  }
  async debug(message: string, ...args: any[]): Promise<string> {
    return this.log("debug", message, ...args);
  }
}
